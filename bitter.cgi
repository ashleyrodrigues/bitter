#!/usr/bin/python2.7

# written by andrewt@cse.unsw.edu.au September 2015
# as a starting point for COMP2041/9041 assignment 2
# http://cgi.cse.unsw.edu.au/~cs2041/assignments/bitter/

import cgi, cgitb, glob, os, re, Cookie, time

# Main method
# Runs through a series of if statements to determine which 
# page to display
def main():
	# Cgi setup
	cgitb.enable()
	dataset_size = "medium"
	users_dir = "dataset-%s/users"% dataset_size
	bleats_dir = "dataset-%s/bleats" %dataset_size
	parameters = cgi.FieldStorage()
	loginFlag = parameters.getvalue('loginFlag', 0)
	valid_login = valid_user(parameters, users_dir)

	# Checks if cookie exists
	try :
		cookie = Cookie.SimpleCookie()
		cookie_data=os.environ.get("HTTP_COOKIE")	
		cookie.load(cookie_data)
		session = cookie["session"].value
		# If session is active, user has already logged in
		if (session != "active"):
			if valid_login:
				cookie["session"] = "active"
				cookie["username"]= parameters.getvalue('username', 0)
				cookie["password"] = parameters.getvalue('password', 0)
				cookie["page"] = "home"
				cookie["pass"] = "f"
				print cookie
				print default_home_page(parameters, cookie,  users_dir, bleats_dir)
				return
			# No one has logged in, display the log in page
			else:
				print default_main_page(parameters, False)
			return
	# If no cookie has been created then no one has logged in
	except (Cookie.CookieError, KeyError, AttributeError):
		# Creates a new cookie and test parameters if login
		# details are valid
		cookie = Cookie.SimpleCookie()
		if valid_login:
			cookie["session"] = "active"
			cookie["username"] = parameters.getvalue('username')
			cookie["password"] = parameters.getvalue('password')
			cookie["page"] = "home"
			cookie["pass"] = "f"
			print cookie
			username = cookie["username"].value
			user_file_data = os.path.join(users_dir, username)
			print default_home_page(parameters, cookie, users_dir, bleats_dir)
			return
		else:
			cookie["session"] = "inactive"
			print cookie
			print page_header()
			print login_page(parameters, True)
			print page_trailer(parameters)
			return

	# Safety net: if session is not active
	if (cookie["session"].value != "active"):
		return

	curr_user_dir = os.path.join(users_dir, cookie["username"].value)
	
	# Series of if statements determines appropriate page by using cookie value

	# Bleat sent page
	if (cookie["page"].value == "new_bleat"):
		if (parameters.getvalue('new_bleat_post', 0)):
			print page_header()
			print notification_bar()
			print "<h2>Created new bleat</h2>"
			create_new_bleat(parameters, cookie["username"].value, users_dir, bleats_dir)
			print page_trailer(parameters)	
			return
	# Reply bleat sent page
	elif (cookie["page"].value == "reply_bleat"):
		if (parameters.getvalue('new_bleat_post', 0)):
			print page_header()
			print notification_bar()
			reply_to_bleat(parameters, cookie["username"].value, users_dir, bleats_dir, cookie["reply_details"].value)
			print "<h2>Your reply was sent</h2>"
			print page_trailer(parameters)
			return	
	# Showing the profile of other users
	elif (cookie["page"].value == "search_content"):
		if (parameters.getvalue('show_profile', 0)):
			print page_header()
			print notification_bar()
			profile_details = parameters.getvalue('show_profile', 0)
			match = re.match('User:\s+(\w+)\s+', profile_details)
			if match:
				print user_page(match.group(1), users_dir)
			else:
				print "<h2>Sorry an error occurred</h2>"
			print page_trailer(parameters)	
			return		

	# Notification bar buttons and textfield
	
	# Shows home page
	if (parameters.getvalue('home_button', 0)):
		print default_home_page(parameters, cookie, users_dir, bleats_dir)
	# Logs the current user out
	elif (parameters.getvalue('logout_button', 0)):
		cookie = Cookie.SimpleCookie()
		cookie["session"] = "inactive"
		print cookie
		#default_main_page(parameters, False)
		print page_header()
		print login_page(parameters, True)
		print page_trailer(parameters)
		return
	# Displays the current users profile
	elif (parameters.getvalue('profile_button', 0)):
		print page_header()
		print notification_bar()
		print user_page(cookie["username"].value, users_dir)
		print page_trailer(parameters)
	# Creates page to send new bleat
	elif (parameters.getvalue('new_bleat', 0)):
		cookie["page"] = "new_bleat"
		print cookie
		print page_header()
		print notification_bar()
		print new_bleat_page(parameters)
		print page_trailer(parameters)
	# Searches for data. This depends upon the value set
	# in the dropdown box	
	elif (parameters.getvalue('search_button')):
		search_type = parameters.getvalue('search_type', 0)
		cookie["page"] = "search_content"
		print cookie
		print page_header()
		print notification_bar()
		if (search_type == "user"):
			print search_user(parameters, users_dir)
		elif (search_type == "bleat"):
			print search_bleat(parameters, bleats_dir)
		print page_trailer(parameters)
		return
	# Sends reply to a bleat
	elif (parameters.getvalue('reply_button', 0)):
		cookie["page"] = "reply_bleat"
		cookie["reply_details"] = parameters.getvalue('reply_bleat', 0)
		print cookie
		print page_header()
		print notification_bar()
		print new_bleat_page(parameters)
		print page_trailer(parameters)
		return
	# Either follows or unfollows a user
	elif (parameters.getvalue('listen_to', 0)):
		print page_header()
		print notification_bar()
		result = follow_or_unfollow(parameters.getvalue('listen_to', 0), curr_user_dir)
		if result:
			print "<h3>User: %s successfully followed<h3>" % parameters.getvalue('listen_to', 0)
		else:
			print "<h3>User: %s successfully unfollowed<h3>" % parameters.getvalue('listen_to', 0)
		print page_trailer(parameters)
		return
	else:
		print default_home_page(parameters, cookie, users_dir, bleats_dir)
		return

# Page to display when no user is logged in
# Login page
def default_main_page (parameters, valid_login):
	html = page_header()
	html += login_page(parameters, valid_login)
	html += page_trailer(parameters)
	return html

# Shows users home page
# Consists of current users profile and their relevant bleats
def default_home_page(parameters, cookie, users_dir, bleats_dir):
	username = cookie["username"].value
	html = ""
	first_pass = cookie["pass"].value
	# If this is the first time opening the home page
	# Sort existing data into personal format
	if (first_pass == "f"):
		cookie["pass"] = "s"
		page_pass = True
		update_idols(username, users_dir)
		print cookie
	else:
		first_pass = False

	html += page_header()
	html += notification_bar()
	html += user_page(username, users_dir)
	user_file_data = os.path.join(users_dir, username)
	
	# Displays relevant bleats	
	if (first_pass):
		relevant_bleats(username, users_dir, bleats_dir)
	ordered_bleats = get_ordered_bleats(username, users_dir, bleats_dir)	
	for bleat in ordered_bleats:
		html += show_bleat(bleat, bleats_dir)	
	html += page_trailer(parameters)
	return html

# Returns 1 if the username is followed by the current
#  user else returns a 0
def follow_or_unfollow(username, curr_user_dir):
	user_followers = os.path.join(curr_user_dir, 'idols.txt')
	if os.path.isfile(user_followers):
		with open(user_followers, 'r') as f:
			details = [line.rstrip('\n') for line in f]
		remove = False
		for line in details:
			match = re.match(username, line)
			if match:
				remove = True
	else:
		remove = False
	if remove:
		f = open(user_followers, 'w')
		for line in details:
			if line != username:
				f.write(line + "\n")
		f.close()
		return 0
	if not remove:
		f =open(user_followers, 'a')
		f.write(username + "\n")
		f.close()
	return 1 

# Called on the first time the home page is accessed
# This function sorts the existing data into
# a certain format and stores the users personal bleats
# and replies to the current user in relevant.txt
def relevant_bleats (username, users_dir, bleats_dir):
	relevant_bleats = []
	bleat_to_epoch = {}
	# Retrieves bleats in which the current user is referenced (replies)
	for f_bleat in os.listdir(bleats_dir):
		bleat_loc = os.path.join(bleats_dir, f_bleat)
		with open(bleat_loc, 'r') as f:
			details = f.readlines()
		password = ""
		for line in details:
			match = re.match('bleat:\s+@([^\s]+)', line)
			if (match):
				if match.group(1) == username:
					relevant_bleats.append(f_bleat)		
		f.close()

	user_dir = os.path.join(users_dir, username)

	# Stores the users that the current user listens to in idols.txt
	update_idols(username, users_dir)

	# Gets bleats which the current user has written
	curr_user_bleats_loc = os.path.join(user_dir, 'bleats.txt')
	with open(curr_user_bleats_loc, 'r') as f:
		curr_user_bleats = [line.rstrip('\n') for line in f]
	relevant_bleats += curr_user_bleats
	# Writes to relevant.txt bleats file in curr users directory				
	relevant_user_bleats = os.path.join(user_dir, 'relevant.txt') 
	with open(relevant_user_bleats, 'w') as f:
		f.write("")
	f = open(relevant_user_bleats, 'a')
	for bleat in relevant_bleats:
		f.write(bleat+"\n")
	f.close()	

# Called when the user logins in for the first time
# Adds all users the current user listens to in
# idols.txt
def update_idols (username, users_dir):
	user_dir = os.path.join(users_dir, username)
	user_details = os.path.join(user_dir, 'details.txt')
	with open(user_details, 'r') as f:
		details = f.readlines()
	
	for line in details:
		match = re.match('listens:\s+(.*)', line)
		if match:
			listens_to = match.group(1)
			listens_to =re.split('\s+', listens_to)
			user_listens_to = os.path.join(user_dir, 'idols.txt')
			f = open(user_listens_to, 'w')
			for user in listens_to:
				f.write(user + "\n")
			f.close()

# Returns a list of the bleats of the user
# the current user listens to
def get_idols_bleats(username, users_dir):
	user_dir = os.path.join(users_dir, username)
	idols_file = os.path.join(user_dir, 'idols.txt')
	with open(idols_file, 'r') as f:
		idols = [line.rstrip('\n') for line in f]	
	relevant_bleats = []
	for user in idols:
		l_user_dir = os.path.join(users_dir, user)
		l_user_bleats_loc = os.path.join(l_user_dir, 'bleats.txt')
		with open(l_user_bleats_loc, 'r') as f:
			l_user_bleats = [line.rstrip('\n') for line in f]
		relevant_bleats += l_user_bleats
	return relevant_bleats

# Returns a list of bleats in reverse chronlogical ordrer
# according to epoch time
# Merges relevant.txt bleats (replies and curr_users bleats)
# with bleats of users the current user listens to
def get_ordered_bleats(username, users_dir, bleats_dir):
	user_dir = os.path.join(users_dir, username)
	relevant_bleats = os.path.join(user_dir, 'relevant.txt')
	with open (relevant_bleats, 'r') as f:
		bleats = [line.rstrip('\n') for line in f]
	bleats += get_idols_bleats(username, users_dir)	
	bleat_to_epoch = {}
	for bleat in bleats:
		bleat_loc = os.path.join(bleats_dir, bleat)
		#bleat_loc = bleat_loc.rstrip('\0')
		if os.path.isfile(bleat_loc):
			with open(bleat_loc, 'r') as f:
				bleat_details = f.readlines()
				for line in bleat_details:
					match = re.match('time:\s+(\d+)', line)
					if match:
						bleat_to_epoch[bleat] = match.group(1)
	
	res = list(sorted(bleat_to_epoch, key=bleat_to_epoch.__getitem__, reverse=True))
	f = open('test.txt', 'w')
	f.write("\n".join(res))
	f.close()
	return res

# Returns formatted html of a bleat given bleat id
def show_bleat(bleat_id, bleats_dir):
	user_data = ""
	user_data_hash = {}
	user_data += """
		<div class="user_bleat">
		<hr>
		<form>
		"""
	bleat_loc = os.path.join(bleats_dir, bleat_id)
	if not os.path.isfile(bleat_loc):
		return
	with open (bleat_loc, 'r') as f:
		details = f.readlines()
	for line in details:
		match = re.match(r'(\w+):\s+(.*)', line)
		if match:
			user_data_hash[match.group(1)] = match.group(2)
	user_data_hash["bleat"] = re.sub(r'^@\w+\s?', '', user_data_hash["bleat"], 1)	
	user_data += '<b>' + user_data_hash["username"] + '</b>'
	user_data += ': <input type="submit" name="listen_button" value="Listen|Unlisten">' 
	user_data += """<input type="hidden" name="listen_to" value="%s"> 
				""" %  (user_data_hash["username"])
	user_data += "<br><i>"
	user_data += user_data_hash["bleat"]+"</i><br>"
	time = ""
	try:
		user_data = time.strftime('%Y-%m-%d', time.localtime(float(user_data_hash["time"])))
	except (ValueError, AttributeError):
		time = user_data_hash["time"]
	user_data += time 
	user_data += '<br><input type="submit" name="reply_button" value="reply">'
	reply_button = '<input type="hidden" name="reply_bleat" value="'
	reply_button += bleat_id+"|"
	reply_button += user_data_hash["username"]
	reply_button += '">'
	user_data += reply_button
	user_data += "</form></div><br>"
	return user_data

# Returns formatted html of the users profile
# Called in home page or when the user selects
# to view a profile
def user_page(user_to_show, users_dir):
	if user_to_show is False:
		return
	else:
		user_filepath = os.path.join(users_dir, user_to_show)
		details_filename = os.path.join(user_filepath, "details.txt")
		user_data = {}
		if not os.path.isfile(details_filename):
			return
		with open(details_filename, 'r') as f:
			details = f.readlines()
		password = ""
		for line in details:          
			match = re.match('password:\s+([^\s]+)', line)
			if (match):
				password = match.group(1)
		for line in details:
			match = re.match("(\w+):\s+(.*)", line)
			if match:
				user_data[match.group(1)] = match.group(2)
	order = ('username', 'full_name', 'email', 'home_suburb', 'listens', 'home_latitude', 'home_longitude')	 
	user_details = ""
	for field in order:
		if field in user_data:
			if field == "listens":
				users_idols = os.path.join(user_filepath, 'idols.txt')
				with open(users_idols, 'r') as f:
					details = [line.rstrip('\n') for line in f]
					user_details += field.upper() + ": " + " ".join(details) + "\n"
			else:
				user_details +=  field.upper() +": " + user_data[field] + "<br>"			
	html = """
	<div class="user_details">
	<h2>Profile: %s</h2>
	""" % user_to_show
	profile_img = ""
	profile_img_loc = os.path.join(user_filepath, 'profile.jpg')
	if os.path.isfile(profile_img_loc):
		html += """<img src="%s">""" % profile_img_loc
	html += """	
    %s
	</div>
    
    """ % (user_details)
	return html 


# Page for user to create a new bleat or reply to a bleat
def new_bleat_page(parameters):
	html = """<div class="new_bleat">
	<h2>Create a new Bleat</h2>
	<form>
		<textarea name=new_bleat_text cols="60" rows="10" maxlength="143"></textarea>
		<br>
	"""
				 
	html += """<input type=submit name="new_bleat_post" value="Post">
	</form>
	</div>		
	"""
	return html
	
# Backend function which creates the new bleat
# Assigns bleat id related to epoch time + offset
# Apppends bleat to relevant.txt
def create_new_bleat(parameters, username, users_dir, bleats_dir):
	bleat_text = parameters.getvalue('new_bleat_text', 0)
	bleat_id = long(time.time()) + 2000000000
	user_dir = os.path.join(users_dir, username)
	bleat_loc = os.path.join(bleats_dir, str(bleat_id))
	bleats_hist = os.path.join(user_dir, "bleats.txt")
	#bleats = os.listdir(bleats_dir)
	#new_bleat_id = max(bleats) + 1
	f = open(bleat_loc, 'a')
	f.write("username: " + username + "\n")
	f.write("bleat: " + bleat_text + "\n")
	f.write("time: " + str(long(time.time())) + "\n")
	f.close()

	f = open(bleats_hist, 'a')
	f.write(str(bleat_id) + "\n")	
	f.close()
	
	bleats_relev = os.path.join(user_dir, "relevant.txt")
	f = open(bleats_relev, 'a')
	f.write(str(bleat_id) + "\n")
	f.close()
	
# Backend function which creates the new bleat
# Assigns bleat id related to epoch time + offset
# Apppends bleat to relevant.txt
# Furthermore appends bleat to relevant bleat of the
# user who wrote the original bleat
def reply_to_bleat(parameters, username, users_dir, bleats_dir, reply_details):

	bleat_text = parameters.getvalue('new_bleat_text', 0)
	bleat_id = long(time.time()) + 2000000000
	user_dir = os.path.join(users_dir, username)
	bleat_loc = os.path.join(bleats_dir, str(bleat_id))
	bleats_hist = os.path.join(user_dir, "bleats.txt")
	
	# First part contains bleat num, second part contains username
	orig_bleat = reply_details.split('|')
	
	f = open(bleat_loc, 'a')
	f.write("username: " + username + "\n")
	f.write("bleat: " + "@" + orig_bleat[1] +" " + bleat_text + "\n")
	f.write("time: " + str(long(time.time())) + "\n")
	f.write("reply_to:" + orig_bleat[0] + "\n")
	f.close()
	
	orig_bleat_user = os.path.join(users_dir, orig_bleat[1])
	orig_bleat_users_bleats = os.path.join(orig_bleat_user, 'bleats.txt')
	f = open(orig_bleat_users_bleats, 'a')
	f.write(str(bleat_id) + "\n")
	f.close()

	f = open(bleats_hist, 'a')
	f.write(str(bleat_id))
	f.close()

	bleats_relev = os.path.join(user_dir, "relevant.txt")
	f = open(bleats_relev, 'a')
	f.write(str(bleat_id) + "\n")
	f.close()



# Html page header
def page_header():
    return """Content-Type: text/html

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bitter</title>
	<link rel="stylesheet" type="text/css" href="bitter.css">
</head>
<body>
	<div class="heading-zero">Server Active</div>
	<div class="heading-one"></div>
		<div class ="heading-two">
			<h1>Bitter</h1>
		</div>
			<div class ="left-container">.</div>
			<div class="mainContainer">
"""

# Checks whether username and password in parameters
# (passeed by cgi) is valid
# Returns true if valid else false
def valid_user(parameters, users_dir):
	user_to_show = parameters.getvalue('username', False)
	if not user_to_show:
		return False
	user_filepath = os.path.join(users_dir, user_to_show)
	details_filename = os.path.join(user_filepath, "details.txt")
	if not os.path.isfile(details_filename):
		f =open('test.txt', 'w')
		f.write(details_filename)
		f.close()
		return False
	with open(details_filename) as f:
		details = f.readlines()
	password = ""
	for line in details:
		match = re.match('password:\s+([^\s]+)', line)
		if (match):
			password = match.group(1)
	if not (parameters.getvalue('password', 0) == password):
		return False
	return True
	
# html of notification bar displayed at the top of the page
def notification_bar():
    html = """
    <div class="menuBar">
		<ul>
		<form>
			<li><input type=submit name="home_button" value="Home"></li>
			<li><input type=submit name="profile_button" value="Profile"></li>
			<li><input type=textfield name="search_content"></li>
			<li>
				<select name="search_type">
					<option value="user">User</option>
					<option value="bleat">Bleat</option>
				</select>
			</li>
			<li><input type=submit name="search_button" value="Search"></li>
			<li><input type=submit name="new_bleat" value="Bleat++"></li>
			<li><input type=submit name="logout_button" value="logout"></li>
		</form>
		</ul>         
    </div>
    """
    return html


def search_bleat (parameters, bleats_dir):
	bleats = os.listdir(bleats_dir)
	search_string = parameters.getvalue('search_content', 0)
	html = ""
	if not search_string:
		return
	for bleat in bleats:
		bleat_loc = os.path.join(bleats_dir, str(bleat))
		bleat_contents = {}
		found = False
		with open(bleat_loc, 'r') as f:
			details = f.readlines()			
			for line in details:
				match = re.match('(\w+):\s+(.*)', line)
				if match:
					bleat_contents[match.group(1)] = match.group(2)
		if not 'bleat' in bleat_contents:
			continue
		else:
			bleat_text = bleat_contents["bleat"]
		if re.search(search_string, bleat_text):
			found = True
		if found:
			html += show_bleat(bleat, bleats_dir)
	return html

# Given search details through cgi, searches for a user
# by username or fullname
# Returns search results as html  	
def search_user(parameters, users_dir):
	users = os.listdir(users_dir)
	search_string = parameters.getvalue('search_content', 0)
	if not search_string:
		return
	search_results = []
	search_results.append("<form>")
	for user in users:
		found = False
		user_dir = os.path.join(users_dir, user)
		user_details = os.path.join(user_dir, 'details.txt')
		if re.search(search_string, user):
			match_text= user
			found = True
		else:
			with(open(user_details, 'r')) as f:
				details = f.readlines()
			for line in details:
				match = re.match('full_name:\s+(\w+)', line)
				if match:
					match_text = match.group(1)
					if re.search(search_string, match.group(1)):
						found = True
		if found:
			html = '<input type="submit" name="show_profile" value="User: '
			html += user
			html += " | Match: "
			html += str(match_text)
			html += '">'
			search_results.append(html)

	search_results.append("</form>")
	return "<br>".join(search_results)

# Login page html
# Called when there is no user loggged in
def login_page(parameters, valid_login):
    html = """
    <input type="hidden" name="loginFlag" value="F">
    <div class="login_details">
	<br>
	<h2>Login</h2>
    <form>
        Username:<br>
        <input type="textfield" name="username"><br>
        Password:<br>
        <input type="password" name="password"><br>
        <input type="submit" value="submit">
    """
    if not (valid_login):
        html += """
            <br>
            <i>Invalid username or password</i>
            """ 
    html += """                
    </form>
    </div>
    """
    return html

#
# HTML placed at the bottom of every page
# It includes all supplied parameter values as a HTML comment
# if global variable debug is set
#
def page_trailer(parameters):
    html = ""
    if debug:
        html += "".join("<!-- %s=%s -->\n" % (p, parameters.getvalue(p)) for p in parameters)
	html += """
			</div>
		<div class ="right-container"></div>	"""
    html += "</body>\n</html>"
    return html

if __name__ == '__main__':
    debug = 1
    main()

